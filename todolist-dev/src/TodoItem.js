/**
 * Created by imac13 on 18/12/22.
 */
import React, {Component} from 'react'
import './style.css'
class TodoItem extends Component {
    constructor (props) {
        super(props)
        this.ctrlFather = this.ctrlFather.bind(this)
    }
    shouldComponentUpdate(nextProps,nextState) {
        if(nextProps.content !== this.props.content) {
            return true
        }else {
            return false
        }
    }
    render () {
        const { content } = this.props
        return (
            <div className="child" onClick = {this.ctrlFather}>
                {content}
            </div>
        )
    }
    ctrlFather () {
        const { deleItem, index } = this.props
        deleItem(index)
    }
}
export default TodoItem