/**
 * Created by imac13 on 18/12/25.
 */
export const CHANGE_INPUT_VALUE = 'change_input_value';
export const CHANGE_ITEM = 'change_item';
export const DELETE_LIST_ITEM = 'detele_list_item';
export const ADD_DATA = 'add_data';
export const GET_INIT_LIST = 'get_init_list';