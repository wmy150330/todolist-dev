/**
 * Created by imac13 on 18/12/25.
 */
import {CHANGE_INPUT_VALUE,CHANGE_ITEM,DELETE_LIST_ITEM,ADD_DATA,GET_INIT_LIST} from './ActionTypes'
export const getInputChangeAction = (value)=> ({
    type:CHANGE_INPUT_VALUE,
    value
});
export const getItemChangeAction = ()=> ({
    type:CHANGE_ITEM,
});
export const getDeleteItemAction = (index)=> ({
    type:DELETE_LIST_ITEM,
    index
});
export const getDeskData = (data)=> ({
    type:ADD_DATA,
    data
});
// export const getTodoList = ()=> {
//     return (dispatch) => {
//
//     }
// };
export const getInitList = ()=> ({
    type:GET_INIT_LIST
})

