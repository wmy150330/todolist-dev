/**
 * Created by imac13 on 18/12/24.
 */
import {CHANGE_INPUT_VALUE,CHANGE_ITEM,DELETE_LIST_ITEM,ADD_DATA} from './ActionTypes'
const defaultState = {
    inputValue:'',
    list: []
}
export default (state = defaultState,action)=>{
    if(action.type === CHANGE_INPUT_VALUE) {
        const newState = JSON.parse(JSON.stringify(state));
        newState.inputValue  = action.value;
        return newState
    }
    if(action.type === CHANGE_ITEM) {
        const newState = JSON.parse(JSON.stringify(state));
        newState.list.push(newState.inputValue);
        newState.inputValue ='';
        return newState
    }
    if(action.type === DELETE_LIST_ITEM){
        const newState = JSON.parse(JSON.stringify(state));
        newState.list.splice(action.index,1)
        return newState
    }
    if(action.type === ADD_DATA){
        const newState = JSON.parse(JSON.stringify(state));
        newState.list = action.data;
        return newState
    }
    return state
}