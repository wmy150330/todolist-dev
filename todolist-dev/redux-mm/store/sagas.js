/**
 * Created by imac13 on 18/12/26.
 */
import { takeEvery,put } from 'redux-saga/effects'
import axios from 'axios'
import { GET_INIT_LIST } from './ActionTypes'
import { getDeskData } from './actionCreators'

function* getInitList() {
    try {
        const res = axios.get('/todolist.json');
        const action = getDeskData(res.data);
        yield put(action)

    }catch (e) {
        console.log('error请求不到')
    }
}
function* mySaga() {
    yield takeEvery(GET_INIT_LIST, getInitList);
}

export default mySaga;