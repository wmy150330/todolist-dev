/**
 * Created by imac13 on 18/12/25.
 */
import React from 'react';
import { Input,Button,List } from 'antd';

const TodoListUI = (props)=> {
    return(
        <div>
            <div style = {{marginLeft: '50px',marginTop: '20px'}}>
                <Input
                    value={props.inputValue}
                    placeholder={"todo info"}
                    style = {{width: '500px',marginRight: '10px'}}
                    onChange = {props.handleInputChange}
                />
                <Button type="primary" onClick={props.handleItem}>提交</Button>
            </div>
            <List style = {{width: '500px',marginTop: '20px',marginLeft: '50px'}}
                  bordered
                  dataSource={props.list}
                  renderItem={(item,index) => (<List.Item onClick={() => {props.deleteItem(index)}}>{item}</List.Item>)}
            />
        </div>
    )
}

export default TodoListUI