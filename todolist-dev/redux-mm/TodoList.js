/**
 * Created by imac13 on 18/12/24.
 */
import React, { Component } from 'react';
import 'antd/dist/antd.css';
import store from './store/index';
import TodoListUI from './TodoListUI'
import { getInputChangeAction, getItemChangeAction, getDeleteItemAction,getInitList } from './store/actionCreators';


class TodoList extends Component{
    constructor(props){
        super(props);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleItem = this.handleItem.bind(this);
        this.handleStoreChange = this.handleStoreChange.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.state = store.getState();
        store.subscribe(this.handleStoreChange)
    }
    render () {
        return(
            <TodoListUI
                inputValue = {this.state.inputValue}
                handleInputChange = {this.handleInputChange}
                handleItem = {this.handleItem}
                list = {this.state.list}
                deleteItem = {this.deleteItem}
            />
        )
    }
    componentDidMount () {
        const action = getInitList()
        store.dispatch(action)
    }

    handleInputChange (e) {
        const action = getInputChangeAction(e.target.value)
        store.dispatch(action)
    }
    handleItem () {
        const action = getItemChangeAction()
        store.dispatch(action)
    }
    handleStoreChange () {
        // console.log(this.state)
        this.setState(store.getState())
    }
    deleteItem (index) {
        const action = getDeleteItemAction(index)
        store.dispatch(action)
    }

}

export default TodoList